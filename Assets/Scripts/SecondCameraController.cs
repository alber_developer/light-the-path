﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class SecondCameraController : MonoBehaviour
{
    public float rotateSpeed = 180f;

    private float inputHorizontalRotation;
    // Update is called once per frame
    void Update()
    {
        inputHorizontalRotation = 0;
        
        //Rotacion 'continua'
        
        if(Input.GetKey(KeyCode.G))
        {
            inputHorizontalRotation = 1;
        } else if(Input.GetKey(KeyCode.H))
        {
            inputHorizontalRotation = -1;
        }
        
        transform.Rotate(Vector3.up * rotateSpeed * Time.deltaTime * inputHorizontalRotation);
        
        
    }
}
